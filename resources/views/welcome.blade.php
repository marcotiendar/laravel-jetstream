<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Estilos de TailWind -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>

    @php
        $color = 'red';
    @endphp

    <body>
        <div class="container mx-auto">
            <x-alert :color="$color" class="mb-4">
                <x-slot name="title">
                    Título 1
                </x-slot>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, nostrum!
            </x-alert>

            <x-alert>
                <x-slot name="title">
                    Título 2
                </x-slot>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem consectetur tempora nesciunt nostrum impedit.
            </x-alert>

            <x-alert>
                <x-slot name="title">
                    Título 3
                </x-slot>
                Que chingue a su madre la 4T
            </x-alert>
        </div>
    </body>
</html>
